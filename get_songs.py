DIR = '/home/john/Documents/py/artist_server'
# ALTERNATIVE_GENRE_URLS = ['https://web.archive.org/web/20140821014158/http://www.artistserver.com/Alternative',
                            # 'https://web.archive.org/web/20140821015019/http://www.artistserver.com/Goth',
                            # 'https://web.archive.org/web/20140821014915/http://www.artistserver.com/Leftfield',
                            # 'https://web.archive.org/web/20140821014925/http://www.artistserver.com/AlternativeCountry',
                            # 'https://web.archive.org/web/20140821015039/http://www.artistserver.com/Grunge',
                            # 'https://web.archive.org/web/20140821015050/http://www.artistserver.com/Punk',
                            # 'https://web.archive.org/web/20140821015046/http://www.artistserver.com/PostRock',
                            # 'https://web.archive.org/web/20140821015048/http://www.artistserver.com/Indie',
                            # 'https://web.archive.org/web/20140821014832/http://www.artistserver.com/NewWave']
# BLUES_GENRE_URLS = ['https://web.archive.org/web/20140821014843/http://www.artistserver.com/AcousticBlues',
                    # 'https://web.archive.org/web/20140821014843/http://www.artistserver.com/Blues',
                    # 'https://web.archive.org/web/20140821014843/http://www.artistserver.com/BluesRock',
                    # 'https://web.archive.org/web/20140821014843/http://www.artistserver.com/BluesVocals',
                    # 'https://web.archive.org/web/20140821014843/http://www.artistserver.com/ElectricBlues'
                    # ]
GENRE_URL = 'https://web.archive.org/web/20140821014158/http://www.artistserver.com/Alternative'
HOME_URL = 'https://web.archive.org/web/20140821095257/http://www.artistserver.com/'

import os
os.chdir(DIR)
from bs4 import BeautifulSoup as bs
import requests

response = requests.get(GENRE_URL)
html = response.content
soup = bs(html, 'lxml')
anchors = soup.find_all('a')
links = []
for tag in anchors:
    links.append(tag['href'])
pageLinks = []
for url in links:
    if 'startrow' in url:
        pageLinks.append(url)

def getFullPageList():
    firstSongOnLastPage = 1
    for i in pageLinks:
        songNum = int(i.split('/')[-1])
        if songNum > firstSongOnLastPage:
            firstSongOnLastPage = songNum  
            print(str(firstSongOnLastPage))
    allCompletePageLinks = []
    while firstSongOnLastPage >= 1:
        partial = pageLinks[0].split('/')[:-1]
        partial.append(str(firstSongOnLastPage))
        fullLink = HOME_URL + '/'.join(partial)
        allCompletePageLinks.append(fullLink)
        firstSongOnLastPage -= 15
    print(allCompletePageLinks)
    return allCompletePageLinks
    
def getSongsOnPage(url, saveFile=True):
    response = requests.get(url)
    html = response.content
    soup = bs(html, 'lxml')
    mp3Anchors = soup.find_all('a', class_='slignore')
    mp3Links = []
    for a in mp3Anchors:
        mp3Links.append(a['href'])
    for mp3 in mp3Links:
        print("'" + mp3 + "'")
        if saveFile: # save each MP3
            fileName = mp3.split('/')[-1]
            mp3File = open(fileName, 'wb')
            r = requests.get(mp3, allow_redirects=True)
            mp3File.write(r.content)
            mp3File.close()
         
allCompletePageLinks = getFullPageList()
getSongsOnPage(allCompletePageLinks[0])      
    
